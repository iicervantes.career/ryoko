
select * from Users;
insert into Users(FirstName, LastName) values('Test', 'User');

select * from CarDetails;
insert into CarDetails(Make, Model) values('Subaru', 'BRZ');

select * from CarExpense;
insert into CarExpense(UserId, Date, RentalCompany, CarDetailId, Mileage, Cost) values(1, '06/22/2016', 'RentalCompany1', 1, 1000, 2.99);
insert into CarExpense(UserId, Date, RentalCompany, CarDetailId, Mileage, Cost) values(1, '06/23/2016', 'RentalCompany1', 1, 1000, 2.99);
insert into CarExpense(UserId, Date, RentalCompany, CarDetailId, Mileage, Cost) values(1, '06/24/2016', 'RentalCompany1', 1, 1000, 2.99);

select * from PlaneExpense;
insert into PlaneExpense(UserId, Date, FlightCompany, Coach, ToCity, FromCity, Cost) values(1, '06/22/2016', 'FlightCompany1', 0, 'to', 'from', 10.99);
insert into PlaneExpense(UserId, Date, FlightCompany, Coach, ToCity, FromCity, Cost) values(1, '06/23/2016', 'FlightCompany1', 0, 'to', 'from', 10.99);
insert into PlaneExpense(UserId, Date, FlightCompany, Coach, ToCity, FromCity, Cost) values(1, '06/24/2016', 'FlightCompany1', 1, 'to', 'from', 10.99);

select * from TrainExpense;
insert into TrainExpense(UserId, Date, TrainCompany, ToCity, FromCity, Cost) values(1, '06/22/2016', 'TrainCompany', 'to', 'from', 6);
insert into TrainExpense(UserId, Date, TrainCompany, ToCity, FromCity, Cost) values(1, '06/23/2016', 'TrainCompany', 'to', 'from', 10);
insert into TrainExpense(UserId, Date, TrainCompany, ToCity, FromCity, Cost) values(1, '06/24/2016', 'TrainCompany', 'to', 'from', 23);

select * from Clients;
insert into Clients(Name) values('Test Client');

select * from ErrorLogs;