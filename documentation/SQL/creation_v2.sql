

create database Ryoko;
use Ryoko;


create table Clients
(
  Id int primary key identity(1,1) not null,
  Name nvarchar(64) not null
)

create table CarDetails
(
  Id int primary key identity(1,1) not null,
  Make nvarchar(64) not null,
  Model nvarchar(64) not null,
)

create table CarExpense
(
  Id int primary key identity(1,1) not null,
  UserId nvarchar(128) foreign key references AspNetUsers(Id),
  ClientId int foreign key references Clients(Id),
  [Date] DateTime not null,
  RentalCompany nvarchar(64) not null,
  CarDetailId int foreign key references CarDetails(Id),
  Mileage int not null,
  Cost float not null
)

create table PlaneExpense
(
  Id int primary key identity(1,1) not null,
  UserId nvarchar(128) foreign key references AspNetUsers(Id),
  ClientId int foreign key references Clients(Id),
  [Date] DateTime not null,
  FlightCompany nvarchar(64) not null,
  Coach bit,
  ToCity nvarchar(64) not null,
  FromCity nvarchar(64) not null,
  Cost float not null
)

create table TrainExpense
(
  Id int primary key identity(1,1) not null,
  UserId nvarchar(128) foreign key references AspNetUsers(Id),
  ClientId int foreign key references Clients(Id),
  [Date] DateTime not null,
  TrainCompany nvarchar(64) not null,
  ToCity nvarchar(64) not null,
  FromCity nvarchar(64) not null,
  Cost float not null
)

create table ErrorLogs
(
  Id int primary key identity(1,1) not null,
  [Message] nvarchar(256) not null,
)


-- deletion if needed

drop table TrainExpense;
drop table PlaneExpense;
drop table CarExpense;
drop table CarDetails;
drop table Clients;
drop table Users;