﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataAccess.DTO
{
    [DataContract]
    public class PlaneDTO : IValidatableObject
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Client { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string FlightCompany { get; set; }

        [DataMember]
        public bool Coach { get; set; }

        [DataMember]
        public string FromCity { get; set; }

        [DataMember]
        public string ToCity { get; set; }

        [DataMember]
        public double Cost { get; set; }

        public static PlaneExpense ConvertFromDTO(PlaneDTO dto)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(dto.Date, DateTimeKind.Local), TimeZoneInfo.Local);
            var result = new PlaneExpense()
            {
                Client = new Client() { Name = dto.Client },
                Cost = dto.Cost,
                Date = convertedTime,
                FromCity = dto.FromCity,
                ToCity = dto.ToCity,
                FlightCompany = dto.FlightCompany,
                UserId = dto.UserId,
                Coach = dto.Coach
            };

            return result;
        }

        public static PlaneDTO ConvertToDTO(PlaneExpense dao)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(dao.Date, DateTimeKind.Utc), TimeZoneInfo.Local);
            var result = new PlaneDTO()
            {
                Client = dao.Client != null ? dao.Client.Name : "",
                Cost = dao.Cost,
                Date = convertedTime,
                FromCity = dao.FromCity,
                ToCity = dao.ToCity,
                FlightCompany = dao.FlightCompany,
                UserId = dao.UserId,
                Coach = dao.Coach ?? false
            };

            return result;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(Client))
            {
                errors.Add(new ValidationResult("Client name is required."));
            }

            if (Date == DateTime.MinValue || Date == DateTime.MaxValue)
            {
                errors.Add(new ValidationResult("Date is required."));
            }

            if (string.IsNullOrWhiteSpace(FlightCompany))
            {
                errors.Add(new ValidationResult("Flight Company is required."));
            }

            if (string.IsNullOrWhiteSpace(ToCity))
            {
                errors.Add(new ValidationResult("To City is required."));
            }

            if (string.IsNullOrWhiteSpace(FromCity))
            {
                errors.Add(new ValidationResult("From City is required."));
            }

            if (Cost <= 0)
            {
                errors.Add(new ValidationResult("Cost is required."));
            }

            return errors;
        }
    }
}