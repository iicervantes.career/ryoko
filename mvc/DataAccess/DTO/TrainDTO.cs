﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataAccess.DTO
{
    [DataContract]
    public class TrainDTO : IValidatableObject
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string Client { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string TrainCompany { get; set; }

        [DataMember]
        public string FromCity { get; set; }

        [DataMember]
        public string ToCity { get; set; }

        [DataMember]
        public double Cost { get; set; }

        public static TrainExpense ConvertFromDTO(TrainDTO dto)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(dto.Date, DateTimeKind.Local), TimeZoneInfo.Local);
            var result = new TrainExpense()
            {
                Client = new Client() { Name = dto.Client },
                Cost = dto.Cost,
                Date = convertedTime,
                FromCity = dto.FromCity,
                ToCity = dto.ToCity,
                TrainCompany = dto.TrainCompany,
                UserId = dto.UserId
            };

            return result;
        }

        public static TrainDTO ConvertToDTO(TrainExpense dao)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(dao.Date, DateTimeKind.Utc), TimeZoneInfo.Local);
            var result = new TrainDTO()
            {
                Client = dao.Client != null ? dao.Client.Name : "",
                Cost = dao.Cost,
                Date = convertedTime,
                FromCity = dao.FromCity,
                ToCity = dao.ToCity,
                TrainCompany = dao.TrainCompany,
                UserId = dao.UserId
            };

            return result;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrWhiteSpace(Client))
            {
                errors.Add(new ValidationResult("Client name is required."));
            }

            if (Date == DateTime.MinValue || Date == DateTime.MaxValue)
            {
                errors.Add(new ValidationResult("Date is required."));
            }

            if (string.IsNullOrWhiteSpace(TrainCompany))
            {
                errors.Add(new ValidationResult("Train Company is required."));
            }

            if (string.IsNullOrWhiteSpace(FromCity))
            {
                errors.Add(new ValidationResult("From City is required."));
            }

            if (string.IsNullOrWhiteSpace(ToCity))
            {
                errors.Add(new ValidationResult("To City is required."));
            }

            if (Cost <= 0)
            {
                errors.Add(new ValidationResult("Cost is required."));
            }

            return errors;
        }
    }
}