﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataAccess.DTO;
using Ryoko.Models;
using System.Web.Script.Serialization;
using System.Net;
using Microsoft.AspNet.Identity;

namespace Ryoko.Controllers
{
    public class ExpenseController : Controller
    {
        private ExpenseDataHelper _helper = new ExpenseDataHelper(); 

        public ActionResult Add()
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login");
            }
            return View();
        }
        
        public HttpStatusCode AddPlane(PlaneDTO dto)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login");
                    throw new Exception("Unauthorized attempt at adding plane expense.");
                }

                if (dto != null)
                {
                    var errors = dto.Validate(null);
                    if (errors.Any())
                    {
                        return HttpStatusCode.BadRequest;
                    }
                }

                _helper.SavePlaneExpense(dto);
                return HttpStatusCode.OK;
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex.ToString());
                return HttpStatusCode.InternalServerError;
            }
        }

        public HttpStatusCode AddCar(CarDTO dto)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login");
                    throw new Exception("Unauthorized attempt at adding car expense.");
                }

                if (dto != null)
                {
                    var errors = dto.Validate(null);
                    if (errors.Any())
                    {
                        return HttpStatusCode.BadRequest;
                    }
                }

                _helper.SaveCarExpense(dto);
                return HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex.ToString());
                return HttpStatusCode.InternalServerError;
            }
        }
        
        public HttpStatusCode AddTrain(TrainDTO dto)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login");
                    throw new Exception("Unauthorized attempt at adding train expense.");
                }

                if (dto != null)
                {
                    var errors = dto.Validate(null);
                    if (errors.Any())
                    {
                        return HttpStatusCode.BadRequest;
                    }
                }

                _helper.SaveTrainExpense(dto);
                return HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex.ToString());
                return HttpStatusCode.InternalServerError;
            }
        }
        
        public ActionResult Review()
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login");
            }
            return View();
        }
        
        public string Get(string userid)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    Response.Redirect("/Account/Login");
                    throw new Exception("Unauthorized attempt at getting data of user "+userid);
                }

                var expenses = _helper.GetUserExpenses(userid);
                var jsonSerialiser = new JavaScriptSerializer();
                var json = jsonSerialiser.Serialize(expenses);
                return json;
            }
            catch(Exception ex)
            {
                return "Error within the server. Exception: " + ex.ToString();
            }
        }
    }
}