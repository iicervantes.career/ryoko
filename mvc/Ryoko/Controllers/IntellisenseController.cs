﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAccess;
using System.Web.Script.Serialization;
using System.Web.Helpers;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Ryoko.Controllers
{
    public class IntellisenseController : ApiController
    {
        private IntellisenseDataHelper _helper = new IntellisenseDataHelper();
        
        public string Get(string type)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(type))
                {
                    return "";
                }
                else if (type.ToLower().Equals("makemodel"))
                {
                    var details = _helper.GetAllCarDetails();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(details);
                    return json;
                }
                else if (type.ToLower().Equals("client"))
                {
                    var details = _helper.GetAllClients();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(details);
                    return json;
                }
                else if (type.ToLower().Equals("rental"))
                {
                    var details = _helper.GetAllRentalCompanies();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(details);
                    return json;
                }
                else if (type.ToLower().Equals("flight"))
                {
                    var details = _helper.GetAllFlightCompanies();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(details);
                    return json;
                }
                else if (type.ToLower().Equals("train"))
                {
                    var details = _helper.GetAllTrainCompanies();
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(details);
                    return json;
                }
                else
                {
                    return "Unsupported type";
                }
            }
            catch(Exception ex)
            {
                Logger.LogMessage(ex.ToString());
                return "error";
            }
        }
    }
}
