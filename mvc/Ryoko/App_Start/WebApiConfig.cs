﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Ryoko
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Intellisense",
                routeTemplate: "api/intel/{type}",
                defaults: new { Controller = "Intellisense" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
